import numpy as np

class Sgd():
    def __init__(self, global_delta):
        self.global_delta = global_delta
    
    def calculate_update(self, individual_delta, weight_tensor, gradient_tensor):
        delta = individual_delta * self.global_delta
        
        return (weight_tensor - np.multiply(gradient_tensor, delta))


class SgdWithMomentum():
    
    def __init__(self, global_delta, mu):
        self.global_delta = global_delta
        self.mu = mu
        self.v = 0.0
        self.id = self.v+1
        
        
    def calculate_update(self, individual_delta, weight_tensor, gradient_tensor):
        delta = individual_delta * self.global_delta        
        self.v = (self.mu * self.v) - (delta * gradient_tensor)
        
        return weight_tensor + self.v


class Adam():
    def __init__(self, global_delta, mu, rho, epsilon):
        self.global_delta = global_delta
        self.mu = mu
        self.rho = rho
        self.epsilon = epsilon
        self.v = 0.0
        self.r = 0.0
        self.k = 1
        
    def calculate_update(self, individual_delta, weight_tensor, gradient_tensor):
        delta = individual_delta * self.global_delta
        
        self.v = (self.mu * self.v) + np.multiply((1.0 - self.mu), gradient_tensor)
        self.r = (self.rho * self.r) +  np.multiply((1.0 - self.rho), np.square(gradient_tensor))
        
        v = np.true_divide(self.v, 1.0 - np.power(self.mu, self.k))
        r = np.true_divide(self.r, 1.0 - np.power(self.rho, self.k))
        
        self.k += 1
                     
        return weight_tensor - delta * np.true_divide((v + self.epsilon), (np.sqrt(r) + self.epsilon))
    
