from enum import Enum


class Base:
    def __init__(self):
        self.regularizer = None

    def add_regularizer(self, regularizer):
        self.regularizer = regularizer

    def add_weight_norm(self, weights):
        pass

    class Phase(Enum):
        train = 0
        test = 1
        validation = 2