import numpy as np
from Layers.Base import Phase
from Layers import FullyConnected, TanH, Sigmoid
import copy
from time import sleep


class LSTM:
    def __init__(self, input_size, hidden_size, output_size, bptt_length):
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.bptt_length = bptt_length
        self.hidden_states = None
        self.cell_states = None
        self.delta = 1
        self.X = None
        self.y = None
        self.tanh = TanH.TanH()
        self.sigmoid = Sigmoid.Sigmoid()
        self.phase = Phase.train
        self.memory = False
        self.weight_norm = 0

        self.fcl_1 = FullyConnected.FullyConnected(self.input_size + self.hidden_size, self.hidden_size * 4)
        self.fcl_2 = FullyConnected.FullyConnected(self.hidden_size, self.output_size)

        self.weights = self.fcl_1.get_weights()

        self.f = None
        self.i = None
        self.c = None
        self.o = None

        self.Wx_optimizer = None
        self.Wy_optimizer = None

    def toggle_memory(self):
        self.memory = not self.memory

    def initialize(self, weights_initializer, bias_initializer):
        self.fcl_1.initialize(weights_initializer, bias_initializer)

        self.fcl_2.initialize(weights_initializer, bias_initializer)

    def set_optimizer(self, optimizer):
        self.Wx_optimizer = optimizer
        self.Wy_optimizer = copy.deepcopy(optimizer)

    def forward(self, input_tensor):
        batch_size = input_tensor.shape[1]
        if self.hidden_states is None:
            self.hidden_states = np.zeros((self.hidden_size, batch_size + 1))
        if self.cell_states is None:
            self.cell_states = np.zeros((self.hidden_size, batch_size + 1))

        if self.memory is False:
            self.hidden_states[:, -1] = np.zeros(self.hidden_size)
            self.cell_states[:, -1] = np.zeros(self.hidden_size)
        else:
            self.hidden_states[:, -1] = self.hidden_states[:, batch_size - 1]
            self.cell_states[:, -1] = self.cell_states[:, batch_size - 1]

        self.f = np.zeros((self.hidden_size, batch_size))
        self.i = np.zeros_like(self.f)
        self.c = np.zeros_like(self.f)
        self.o = np.zeros_like(self.f)

        self.fcl_1_out = np.zeros((self.hidden_size * 4, batch_size))

        self.y = np.zeros((self.output_size, batch_size))

        self.X = np.zeros((self.hidden_size + self.input_size, batch_size))

        for t in range(batch_size):
            self.X[:, t] = np.concatenate((self.hidden_states[:, t - 1], input_tensor[:, t]))

            self.fcl_1_out[:, t] = self.fcl_1.forward(self.X[:, t])

            sigmoid_tensor = self.sigmoid.forward((self.fcl_1_out[0:self.hidden_size * 3, t]))

            self.f[:, t] = sigmoid_tensor[0:self.hidden_size]
            self.i[:, t] = sigmoid_tensor[self.hidden_size:self.hidden_size * 2]
            self.c[:, t] = self.tanh.forward((self.fcl_1_out[self.hidden_size * 3:self.hidden_size * 4, t]))
            self.o[:, t] = sigmoid_tensor[self.hidden_size * 2:self.hidden_size * 3]

            self.cell_states[:, t] = (self.f[:, t] * self.cell_states[:, t - 1]) + (self.i[:, t] * self.c[:, t])

            self.hidden_states[:, t] = self.o[:, t] * self.tanh.forward(self.cell_states[:, t])

            self.y[:, t] = self.fcl_2.forward(self.hidden_states[:, t])

        return self.y

    def backward(self, error_tensor):
        batch_size = error_tensor.shape[1]
        dh_next = np.zeros_like(self.hidden_states[:, batch_size - 1])
        dc_next = np.zeros_like(self.cell_states[:, batch_size - 1])

        d_x = np.zeros_like(self.X)
        self.d_Wy = np.zeros_like(self.fcl_2.get_weights())

        self.d_Wx = np.zeros_like(self.fcl_1.get_weights())

        k1 = self.bptt_length // batch_size
        k2 = self.bptt_length // batch_size

        for t in np.arange(batch_size)[::-1]:
            self.fcl_2.X = self.hidden_states[:, t]
            d_y = self.fcl_2.backward(error_tensor[:, t])
            self.d_Wy += self.fcl_2.get_gradient_weights()

            dh = dh_next + d_y

            if t % k1 == 0:
                for bptt_step in np.arange(t, t - k2, -1):
                    self.tanh.y = self.tanh.forward(self.cell_states[:, bptt_step])
                    dc = self.tanh.backward(self.o[:, bptt_step] * dh) + dc_next

                    self.sigmoid.y = self.o[:, bptt_step]
                    d_o = self.sigmoid.backward(dh * self.tanh.forward(self.cell_states[:, bptt_step]))

                    self.tanh.y = self.c[:, bptt_step]
                    d_c = self.tanh.backward(dc * self.i[:, bptt_step])

                    self.sigmoid.y = self.i[:, bptt_step]
                    d_i = self.sigmoid.backward(dc * self.c[:, bptt_step])

                    self.sigmoid.y = self.f[:, bptt_step]
                    d_f = self.sigmoid.backward(dc * self.cell_states[:, bptt_step - 1])

                    dc_next = self.f[:, bptt_step] * dc

                    sigmoid_tensor = np.concatenate((d_f, d_i, d_o))

                    self.fcl_1.X = self.X[:, bptt_step]
                    d_x[:, bptt_step] = self.fcl_1.backward(np.concatenate((sigmoid_tensor, d_c)))
                    self.d_Wx += self.fcl_1.get_gradient_weights()

                    dh_next = d_x[0:self.hidden_size, bptt_step]

        # Update weights
        if self.Wx_optimizer:
            self.fcl_1.set_weights(self.Wx_optimizer.calculate_update(self.delta, self.fcl_1.get_weights(), self.d_Wx))

        if self.Wy_optimizer:
            self.fcl_2.set_weights(self.Wy_optimizer.calculate_update(self.delta, self.fcl_2.get_weights(), self.d_Wy))

        self.weights = self.fcl_1.get_weights()

        return d_x[self.hidden_size:self.input_size + self.hidden_size]

    def get_gradient_weights(self):
        return self.d_Wx

    def set_weights(self, weights):
        self.fcl_1.set_weights(weights)

    def get_weights(self):
        return self.fcl_1.get_weights()
