import numpy as np
from Layers.Base import Phase


class FullyConnected:
    def __init__(self, input_size, output_size):
        self.input_size = input_size
        self.output_size = output_size
        self.delta = 1
        self.weights = np.random.rand(self.output_size, self.input_size + 1)
        self.optimizer = None
        self.X = None
        self.gradient_tensor = None
        self.weight_norm = 0
        self.phase = Phase.train

    def initialize(self, weights_initializer, bias_initializer):
        _weights = np.empty([self.output_size, self.input_size])
        weights_initializer.initialize(_weights)
        _bias = np.empty([self.output_size, 1])
        bias_initializer.initialize(_bias)

        self.weights = np.concatenate((_weights, _bias), axis=1)

    def set_optimizer(self, optimizer):
        self.optimizer = optimizer

    def forward(self, input_tensor):
        if len(input_tensor.shape) == 1:
            newrow = np.ones([self.weights.shape[1] - input_tensor.shape[0]])
        else:
            newrow = np.ones([self.weights.shape[1] - input_tensor.shape[0], input_tensor.shape[1]])
        self.X = np.concatenate((input_tensor, newrow))
        if self.optimizer is not None:
            if self.optimizer.regularizer is not None:
                self.weight_norm = self.optimizer.regularizer.norm(self.weights)

        return np.dot(self.weights, self.X)

    def backward(self, error_tensor):
        if len(error_tensor.shape) == 1:
            self.gradient_tensor = np.dot(np.vstack(error_tensor), np.vstack(self.X).T)
        else:
            self.gradient_tensor = np.dot(error_tensor, self.X.transpose())
        _weights = self.weights[:, 0:self.input_size]
        if self.optimizer:
            self.weights = self.optimizer.calculate_update(self.delta, self.weights, self.gradient_tensor)

        return np.dot(_weights.transpose(), error_tensor)

    def get_gradient_weights(self):
        return self.gradient_tensor

    def get_weights(self):
        return self.weights[:, 0:self.input_size]

    def set_weights(self, weights):
        self.weights = weights
