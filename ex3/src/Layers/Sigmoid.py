import numpy as np
from Layers.Base import Phase


class Sigmoid:
    def __init__(self):
        self.weight_norm = 0
        self.phase = Phase.train
        self.y = None

    def forward(self, input_tensor):
        self.y = 1 / (1 + np.exp(-input_tensor))
        return self.y

    def backward(self, error_tensor):
        return (self.y * (1 - self.y)) * error_tensor
