import numpy as np
from Layers.Base import Phase
from numpy import newaxis


class BatchNormalization:
    def __init__(self, channels=0):
        self.channels = channels
        self.phase = Phase.train
        self.X = None
        self.X_norm = None
        self.mu = 0
        self.var = 0
        self.weights = None  # np.vstack(np.ones((x.shape[0])))
        self.bias = None  # np.vstack(np.zeros((x.shape[0])))
        self.weight_gradient = None
        self.bias_gradient = None
        self.weight_norm = 0
        self.delta = 1
        self.optimizer = None

        self.mu_tilde = None
        self.var_tilde = None

    def initialize(self, weights_initializer, bias_initializer):
        pass

    def set_optimizer(self, optimizer):
        self.optimizer = optimizer

    def forward(self, input_tensor):
        # Reshaping input accordingly to the channels
        if self.channels > 0:
            x = input_tensor.reshape(self.channels,
                                     np.int(input_tensor.shape[0] / self.channels) * input_tensor.shape[1])
        else:
            x = input_tensor

        batch_size = x.shape[1]
        x_norm = np.empty_like(x)
        output = np.empty_like(input_tensor)

        # Mean and variance throughout the batches
        if self.phase == Phase.train:
            mu = np.vstack(np.mean(x, axis=1))
            var = np.vstack(np.var(x, axis=1))

            # Calculation of the moving avarage of the mean and variance
            if self.mu_tilde is None:
                self.mu_tilde = mu.copy()
                self.var_tilde = var.copy()
            else:
                self.mu_tilde = 0.8 * self.mu_tilde + 0.2 * mu
                self.var_tilde = 0.8 * self.var_tilde + 0.2 * var

            self.mu = mu
            self.var = var

        # In test time, just put the moving average calculated during training
        if self.phase == Phase.test:
            self.mu = self.mu_tilde
            self.var = self.var_tilde

        if self.weights is None and self.bias is None:
            self.weights = np.vstack(np.ones((x.shape[0])))
            self.bias = np.vstack(np.zeros((x.shape[0])))

        # Normalizing the input
        x_norm = (x - self.mu) / np.sqrt(self.var + 1e-8)

        # Multiplying by weights and adding bias
        output = self.weights * x_norm + self.bias

        if self.channels > 0:
            output = output.reshape(input_tensor.shape)

        self.X = x
        self.X_norm = x_norm

        return output

    def backward(self, error_tensor):
        if self.channels > 0:
            e = error_tensor.reshape(self.channels,
                                     np.int(error_tensor.shape[0] / self.channels) * error_tensor.shape[1])
        else:
            e = error_tensor


        batch_size = e.shape[1]

        # Gradient with respect to weights (gamma)
        self.weight_gradient = np.vstack(np.sum(self.X_norm * e, axis=1))

        # Gradient with respect to bias (beta)
        self.bias_gradient = np.vstack(np.sum(e, axis=1))

        # Subtract input by the mean
        X_mu = (self.X - self.mu)

        # Inverse of the variance (sigma**2)
        var_inv = 1. / np.sqrt(self.var + 1e-8)

        # Gradient with respect to X_norm
        dX_norm = e * self.weights

        # Gradient with respect to variance (sigma**2)
        dvar = np.sum(dX_norm * X_mu * (-0.5 * (self.var + 1e-8) ** (-3 / 2.0)), axis=1)

        # Gradient with respect to mean (mu)
        dmu = np.vstack(np.sum(dX_norm * -var_inv, axis=1) + dvar * (np.sum(-2. * X_mu, axis=1) / batch_size))

        # Gradient with respect to input
        dX = (dX_norm * var_inv) + (dvar[:, newaxis] * 2. * X_mu / batch_size) + (dmu / batch_size)

        # Update weights and bias
        if self.optimizer:
            self.weights = self.optimizer.calculate_update(self.delta, self.weights, self.weight_gradient)
            self.bias = self.optimizer.calculate_update(self.delta, self.bias, self.bias_gradient)

        if self.channels > 0:
            dX = dX.reshape(error_tensor.shape)

        return dX

    def get_gradient_weights(self):
        return self.weight_gradient

    def get_gradient_bias(self):
        return self.bias_gradient

    def get_weights(self):
        return self.weights

    def set_weights(self, weights):
        self.weights = weights
