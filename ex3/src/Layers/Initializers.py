import numpy as np
from Layers.Base import Phase

class Constant:
    def __init__(self, const):
        self.const = const
        self.weight_norm = 0
        self.phase = Phase.train

    def initialize(self, weights):
        if isinstance(weights, int):
            return self.const
        else:
            _weights = np.full(weights.shape, self.const)
            # weights.copyTo(_weights)
            return _weights


class UniformRandom:
    def __init__(self):
        pass
        #self.weight_norm = 0

    def initialize(self, weights):
        _weights = np.random.uniform(0, 1, (weights.shape[0], weights.shape[1]))

        for i in range(weights.shape[0]):
            for j in range(weights.shape[1]):
                weights[i][j] = _weights[i][j]

        return _weights


class Xavier:
    def __init__(self):
        pass
        #self.weight_norm = 0

    def initialize(self, weights):
        fan_in = weights.shape[0] if len(weights.shape) == 2 else np.prod(weights.shape[1:])
        fan_out = weights.shape[1] if len(weights.shape) == 2 else weights.shape[0]

        sig = np.sqrt(2.0 / (fan_in + fan_out))

        _weights = np.random.normal(loc=0.0, scale=sig, size=weights.shape)

        for i in range(weights.shape[0]):
            for j in range(weights.shape[1]):
                weights[i][j] = _weights[i][j]

        return _weights
