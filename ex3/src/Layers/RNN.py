import numpy as np
from Layers.Base import Phase
from Layers import FullyConnected, TanH, Sigmoid
import copy


class RNN:
    def __init__(self, input_size, hidden_size, output_size, bptt_length):
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_size = hidden_size
        self.bptt_length = bptt_length
        self.hidden_states = None
        self.delta = 1
        self.X = None
        self.tanh_input_tensor = None
        self.tanh = TanH.TanH()
        self.sigmoid = Sigmoid.Sigmoid()
        self.fcl_tanh_optimizer = None
        self.fcl_sig_optimizer = None
        self.optimizer = None
        self.weight_norm = 0
        self.weights = np.random.rand(self.hidden_size, self.input_size + self.hidden_size + 1)
        self.weights_sig = np.random.rand(self.output_size, self.hidden_size + 1)
        self.phase = Phase.train
        self.memory = False
        self.weight_norm = 0

    def toggle_memory(self):
        self.memory = not self.memory

    def initialize(self, weights_initializer, bias_initializer):
        _weights = np.empty([self.hidden_size, self.input_size + self.hidden_size])
        weights_initializer.initialize(_weights)
        _bias = np.empty([self.hidden_size, 1])
        bias_initializer.initialize(_bias)
        self.weights = np.concatenate((_weights, _bias), axis=1)

        _weights = np.empty([self.output_size, self.hidden_size])
        weights_initializer.initialize(_weights)
        _bias = np.empty([self.output_size, 1])
        bias_initializer.initialize(_bias)
        self.weights_sig = np.concatenate((_weights, _bias), axis=1)

    def set_optimizer(self, optimizer):
        self.fcl_tanh_optimizer = optimizer
        self.fcl_sig_optimizer = copy.deepcopy(optimizer)

    def calculate_regularization_loss(self):
        pass

    def forward(self, input_tensor):
        batch_size = input_tensor.shape[1]
        if self.hidden_states is None:
            self.hidden_states = np.zeros((self.hidden_size, batch_size+1))

        if self.memory is False:
            self.hidden_states[:, -1] = np.zeros(self.hidden_size)
        else:
            self.hidden_states[:, -1] = self.hidden_states[:, batch_size-1]

        self.X = input_tensor
        self.tanh_input_tensor = np.zeros((self.hidden_size, batch_size+1))
        y = np.zeros((self.output_size, batch_size))
        bias = np.ones(1)

        for t in range(batch_size):
            c = np.concatenate((self.hidden_states[:, t - 1], self.X[:, t], bias))
            self.hidden_states[:, t] = self.tanh.forward(self.weights.dot(c))
            hc = np.concatenate((self.hidden_states[:, t], bias))
            y[:, t] = self.weights_sig.dot(hc)

        return y

    def backward(self, error_tensor):
        batch_size = error_tensor.shape[1]

        self.dW_sig = np.zeros_like(self.weights_sig)
        self.dW_tanh = np.zeros_like(self.weights)

        dw_hy = np.zeros_like(self.weights_sig[:, 0:self.hidden_size])
        db_y = np.zeros_like(self.weights_sig[:, self.hidden_size])

        dw_hh = np.zeros_like(self.weights[:, 0:self.hidden_size])
        dw_xh = np.zeros_like(self.weights[:, self.hidden_size:self.hidden_size + self.input_size])
        db_h = np.zeros_like(self.weights[:, self.hidden_size + self.input_size])

        w_hy = self.weights_sig[:, 0:self.hidden_size]
        w_hh = self.weights[:, 0:self.hidden_size]
        w_xh = self.weights[:, self.hidden_size:self.hidden_size + self.input_size]

        output = np.zeros_like(self.X)

        dh_next = np.zeros_like(self.hidden_states[:, 0])
        dh = np.zeros_like(self.hidden_states[:, 0])

        k1 = self.bptt_length // batch_size
        k2 = self.bptt_length // batch_size

        for t in np.arange(batch_size)[::-1]:
            d_ot = error_tensor[:, t]
            dw_hy += np.dot(np.vstack(d_ot), np.vstack(self.hidden_states[:, t]).T)
            db_y += d_ot
            d_ht = w_hy.T.dot(d_ot) + dh_next

            if t % k1 == 0:
                for bptt_step in np.arange(t, t - k2, -1):

                    self.tanh.y = self.hidden_states[:, bptt_step]
                    dh = self.tanh.backward(d_ht)
                    db_h += dh

                    dw_xh += np.dot(np.vstack(dh), np.vstack(self.X[:, bptt_step]).T)
                    dw_hh += np.dot(np.vstack(dh), np.vstack(self.hidden_states[:, bptt_step - 1]).T)
                    output[:, bptt_step] += np.dot(w_xh.T, dh)
                    dh_next = np.dot(w_hh.T, dh)

        self.dW_sig = np.concatenate((dw_hy, np.vstack(db_y)), axis=1)
        self.dW_tanh = np.concatenate((dw_hh, dw_xh, np.vstack(db_h)), axis=1)

        # Update weights
        if self.fcl_sig_optimizer:
            self.weights_sig = self.fcl_sig_optimizer.calculate_update(self.delta, self.weights_sig, self.dW_sig)

        if self.fcl_tanh_optimizer:
            self.weights = self.fcl_tanh_optimizer.calculate_update(self.delta, self.weights, self.dW_tanh)

        # Return only the the gradient with respect to the inputs
        return output

    def get_gradient_weights(self):
        return self.dW_tanh

    def get_weights(self):
        return self.weights

    def set_weights(self, weights):
        self.weights = weights
