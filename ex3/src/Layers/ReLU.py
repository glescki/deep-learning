import numpy as np
from Layers.Base import Phase

class ReLU:
    def __init__(self):
        self.input = None
        self.weight_norm = 0
        self.phase = Phase.train

    def forward(self, input_tensor):
        self.input = input_tensor
        self.input[input_tensor <= 0] = 0

        return self.input

    def backward(self, error_tensor):
        re_error = error_tensor  # np.dot(error_tensor.T, self.input)

        for i in range(self.input.shape[0]):
            for j in range(self.input.shape[1]):
                if self.input[i][j] <= 0:
                    re_error[i][j] = 0
                else:
                    re_error[i][j] = error_tensor[i][j]

        return re_error
