import numpy as np
from Layers.Base import Phase


class TanH:
    def __init__(self):
        self.phase = Phase.train
        self.y = None

    def forward(self, input_tensor):
        self.y = np.tanh(input_tensor)
        return self.y

    def backward(self, error_tensor):
        return (1 - self.y ** 2) * error_tensor
