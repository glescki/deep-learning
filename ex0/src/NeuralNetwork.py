class NeuralNetwork():
	def __init__(self):  
		self.loss = []
		self.layers = []
		self.data_layer = None
		self.loss_layer = None
    
	def forward(self, activation_tensor):     
		for layer in self.layers[:-1]:
			activation_tensor = layer.forward(activation_tensor)
		return activation_tensor
    
	def backward(self, error_tensor):
		for layer in reversed(self.layers[:-1]):
			error_tensor = layer.backward(error_tensor)
		return error_tensor
    
	def train(self, iterations):
		for i in range(iterations):
			input_tensor, label_tensor = self.data_layer.forward()
			activation_tensor = self.forward(input_tensor)

			self.loss.append(self.loss_layer.forward(activation_tensor, label_tensor))
			error_tensor = self.loss_layer.backward(label_tensor)

			error_tensor = self.backward(error_tensor)

	def test(self, input_tensor):
		activation_tensor = self.forward(input_tensor)
		return self.loss_layer.predict(activation_tensor)
    
    
    
