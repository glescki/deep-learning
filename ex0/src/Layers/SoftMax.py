import numpy as np
import math

class SoftMax():
    def __init__(self):
        self.Y = None
        
    def normalize_matrix(self, input):
        n = np.empty([input.shape[0], input.shape[1]])
        for i in range(input.shape[0]):
            for j in range(input.shape[1]):
                n[i][j] = np.subtract(input[i][j], input[i].max())
        return n
        
    def forward(self, input_tensor, label_tensor):
        self.Y = self.predict(input_tensor)
        
        t_label = label_tensor.transpose()
        t_Y = (self.Y).transpose()
        #print t_label
        loss = 0.0
        for i in range(label_tensor.shape[1]):
            for j in range(label_tensor.shape[0]):
                if t_label[i][j] == 1:         
                    loss += (- np.log(t_Y[i][j]))          
        return loss
    
    def backward(self, label_tensor):
        return self.Y - label_tensor
    
    def predict(self, input_tensor):
        d = []
        n_input = self.normalize_matrix(input_tensor.transpose())
        Y = np.empty([input_tensor.shape[1], input_tensor.shape[0]])
        for i in n_input:
            d.append(np.sum(np.exp(i)))
        for i in range(input_tensor.shape[1]):
            for j in range(input_tensor.shape[0]):          
                Y[i][j] = np.true_divide(np.exp(n_input[i][j]), d[i])
                
        return Y.transpose()
    
    
    