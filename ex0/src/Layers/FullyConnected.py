import numpy as np

class FullyConnected ():    
    def __init__(self, input_size, output_size):
        self.input_size = input_size
        self.output_size = output_size
        self.delta = 1
        self.bias = 1
        self.weights = np.random.rand(self.output_size, self.input_size+1)
        self.X = None
        self.a = None

        
    def forward(self, input_tensor):
        newrow = np.ones([1, input_tensor.shape[1]])        
        self.X = np.concatenate((input_tensor, newrow))
        
        
        return np.dot(self.weights, self.X)
            
    def backward(self, error_tensor):
        _weights = np.delete(self.weights, self.weights.shape[1]-1, axis=1)
        self.a = np.dot(error_tensor, self.X.transpose())
        
        self.weights = (self.weights - np.multiply(self.a, self.delta))
        return np.dot(_weights.transpose(), error_tensor)
    
    def get_gradient_weights(self):
        return self.a
