import os.path

import tensorflow as tf
from datetime import datetime

from Models import AlexNet as model
#from Models import InceptionNet as model
#from Models import DenseNet as model

from Data import input_data
from HelperFunctions import train_helper
from HelperFunctions import models_helper

LEARNING_RATE = 0.0001
MAX_STEPS = 800  # steps = train_steps/batch_size
MAX_VALID_STEPS = 200  # steps = valid_steps/valid_batch_size
MAX_TEST_STEPS = 200  # steps = test_steps/test_batch_size
MAX_EPOCHS = 15
TRAIN_STEPS = 40000  # = how many training images
TEST_STEPS = 10000  # = how many test images
VALID_STEPS = 10000  # = how many validation images
BATCH_SIZE = 50
BATCH_SIZE_TEST = 50
BATCH_SIZE_VALID = 50
OPTIMIZER = 2  # 1 = SGD, 2 = Adam, 3 = RMSProp


def run_training():
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    LOG_DIR = os.path.join(BASE_DIR, 'logs/')
    WEIGHTS_DIR = os.path.join(BASE_DIR, 'trained_models/')
    mean, std = input_data.get_mean_std()

    with tf.Graph().as_default():

        images_placeholder, labels_placeholder = train_helper.placeholder_inputs(BATCH_SIZE, 32, 32, 3, 10)
        batchnorm_placeholder = tf.placeholder(tf.bool)
        mean_normalized_images = tf.subtract(images_placeholder, mean)
        normalized_images = tf.divide(mean_normalized_images, std)

        output = model.forward(normalized_images, batchnorm_placeholder)

        # Loss op, Cross entropy
        with tf.name_scope("cross_ent"):
            loss = models_helper.loss(output, labels_placeholder)

        # Layers to monitor (last two layers of AlexNet)
        train_layers = ['fc1', 'fc2']
        var_list = [v for v in tf.trainable_variables() if v.name.split('/')[0] in train_layers]
        # Get gradients of all trainable variables
        gradients = tf.gradients(loss, var_list)
        gradients = list(zip(gradients, var_list))
        for gradient, var in gradients:
            tf.summary.histogram(var.name + '/gradient', gradient)

        # Add the loss to summary
        tf.summary.scalar('cross_entropy', loss)

        # Accuracy
        with tf.name_scope("accuracy"):
            correct_pred = tf.subtract(tf.argmax(output, 1), tf.argmax(labels_placeholder, 1)) ** 2
            accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

        # Train op
        with tf.name_scope("train"):
            train_op = models_helper.training(loss, LEARNING_RATE, OPTIMIZER)

        # Add the accuracy to the summary
        tf.summary.scalar('accuracy', accuracy)

        summary = tf.summary.merge_all()

        init = tf.global_variables_initializer()
        saver = tf.train.Saver()

        session = tf.Session()
        summary_writer = tf.summary.FileWriter(LOG_DIR, session.graph)

        session.run(init)

        # global_step = tf.Variable(0, name='global_step', trainable=False)
        print('{} Starting training'.format(datetime.now()))
        for epoch in range(MAX_EPOCHS):
            print('EPOCH : ', epoch, ' BEGIN')

            for step in range(MAX_STEPS):
                # to implement
                batch_xs, batch_ys = input_data.next_train_batch(BATCH_SIZE)

                _, loss_value = session.run([train_op, loss], feed_dict={images_placeholder: batch_xs,
                                                                         labels_placeholder: batch_ys,
                                                                         batchnorm_placeholder: 1})
                if step % 100 == 0:
                    # Print status on stdou
                    print('{} Loss on step {}: {}'.format(datetime.now(), step, loss_value))
                    # Save summary
                    summary_str = session.run(summary, feed_dict={images_placeholder: batch_xs,
                                                                  labels_placeholder: batch_ys,
                                                                  batchnorm_placeholder: 1})
                    summary_writer.add_summary(summary_str, step)
                    summary_writer.flush()

                # Save a checkpoint of the model after every epoch.
                if (step + 1) == MAX_STEPS:
                    print('Saving current model state')
                    checkpoint_file = os.path.join(LOG_DIR, 'model.ckpt')
                    saver.save(session, checkpoint_file, epoch*MAX_STEPS + step)
                    # Do evaluation with validation
                    train_helper.do_eval(session, accuracy, images_placeholder, labels_placeholder, batchnorm_placeholder,
                                         True, epoch, VALID_STEPS, BATCH_SIZE_VALID)

            print('EPOCH : ', epoch, ' END')

            print('\n-----------------------------------------------')
            print('Run trained model on test data: ')
        # to implement
            train_helper.do_eval(session, accuracy, images_placeholder, labels_placeholder, batchnorm_placeholder,
                                 False, epoch, VALID_STEPS, BATCH_SIZE_VALID)


if __name__ == '__main__':
    run_training()
