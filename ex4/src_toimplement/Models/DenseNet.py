# DenseNet architectural graph

# Tensorflow imports
import tensorflow as tf

from HelperFunctions import models_helper


# define model parameters here
# ...

def dense_layer(x, channels, growth_rate, training, scope):
    with tf.variable_scope(scope):
        batch_norm = models_helper.batch_norm(x, training, scope+"_batch_norm")
        relu = tf.nn.relu(batch_norm)
        conv3x3 = models_helper.conv2d(relu, channels, growth_rate, 3, 1, 'SAME', True, scope+'_conv3x3')
        x = tf.concat([x, conv3x3], 3)
        print('Shape of x (channels):', x.get_shape().as_list()[3])
        channels += growth_rate
    return x, channels


def dense_block(x, channels, layers, growth_rate, training, scope):
    for l in range(layers):
        x, channels = dense_layer(x, channels, growth_rate, training, scope+"_layer"+str(l))
    y = x
    return y


def dense_transition(x, channels, training, scope):
    with tf.variable_scope(scope):
        batch_norm = models_helper.batch_norm(x, training, scope)
        conv1x1 = models_helper.conv2d(batch_norm, channels, channels, 1, 1, 'SAME', True, scope+'_conv1x1')
        y = tf.nn.avg_pool(conv1x1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
        print('Y shape (channles):', y.get_shape().as_list()[3])
    return y


def forward(images_batch, training):
    with tf.name_scope('densenet'):
        conv1 = models_helper.conv2d(images_batch, 3, 16, 3, 1, 'SAME', True, 'conv1')

        block1 = dense_block(conv1, 16, 12, 12, training, 'block1')
        output_channels = block1.get_shape().as_list()[3]
        trans1 = dense_transition(block1, output_channels, training, 'trans1')

        block2 = dense_block(trans1, output_channels, 12, 12, training, 'block2')

        trans2 = dense_transition(block2, 304, training, 'trans2')

        block3 = dense_block(trans2, 304, 12, 12, training, 'block3')

        pool1 = tf.nn.avg_pool(block3, ksize=[1, 8, 8, 1], strides=[1, 8, 8, 1], padding='VALID')
        print('After pooling', pool1.get_shape().as_list())

        flattened = tf.reshape(pool1, [-1, 1 * 1 * 448])
        fc1 = models_helper.fc(flattened, 1 * 1 * 448, 10, 'fc1', relu=False)

    return fc1
