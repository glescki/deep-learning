# AlexNet architectural graph

# Tensorflow imports
import tensorflow as tf

# Define model parameters here
from HelperFunctions import train_helper
from HelperFunctions import models_helper


def forward(images_batch, training):
    with tf.name_scope('alexnet'):
        # 1st Layer: Conv (w ReLu) -> Pool -> Norm
        conv1 = models_helper.conv2d(images_batch, 3, 64, 5, 1, 'SAME', True, 'conv1')
        pool1 = tf.nn.max_pool(conv1, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')
        norm1 = tf.nn.local_response_normalization(pool1)

        # 2nd Layer: Conv (w ReLu) -> Norm -> Pool
        conv2 = models_helper.conv2d(norm1, 64, 64, 5, 1, 'SAME', True, 'conv2')
        norm2 = tf.nn.local_response_normalization(conv2)
        pool2 = tf.nn.max_pool(norm2, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')

        # 3rd Layer: FC (w ReLu)
        flattened = tf.reshape(pool2, [-1, 8 * 8 * 64])
        fc1 = models_helper.fc(flattened, 8 * 8 * 64, 384, 'fc1')

        # 4th Layer: FC (w ReLu)
        fc2 = models_helper.fc(fc1, 384, 192, 'fc2')

        # 5th Layer: FC (w/o ReLu) -> Softmax
        fc3 = models_helper.fc(fc2, 192, 10, 'fc3', relu=False)

    return fc3
