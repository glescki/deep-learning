# Inception network architectural graph

# Tensorflow imports
import tensorflow as tf

from HelperFunctions import models_helper

# define model parameters here
# ...

  
def inception_block(x, channels_in, filter_1, filter_3, redfilter3, filter_5, redfilter5, redpool, scope):
        
    with tf.variable_scope(scope):
        conv1x1_1 = models_helper.conv2d(x, channels_in, filter_1, 1, 1, 'SAME', False, scope+'_filter1')

        conv1x1_2 = models_helper.conv2d(x, channels_in, redfilter3, 1, 1, 'SAME', False, scope+'_redfilter3')
        conv3x3 = models_helper.conv2d(conv1x1_2, redfilter3, filter_3, 3, 1, 'SAME', False, scope+'_filter3')

        conv1x1_3 = models_helper.conv2d(x, channels_in, redfilter5, 1, 1, 'SAME', False, scope+'_redfilter5')
        conv5x5 = models_helper.conv2d(conv1x1_3, redfilter5, filter_5, 5, 1, 'SAME', False, scope+'_filter5')

        inception_pool = tf.nn.max_pool(x, ksize=[1, 3, 3, 1], strides=[1, 1, 1, 1], padding='SAME')
        conv1x1_4 = models_helper.conv2d(inception_pool, channels_in, redpool, 1, 1, 'SAME', False, scope+'_poolfilter')

        bias = tf.get_variable('bias_conv', shape=[filter_1 + filter_3 + filter_5 + redpool],
                               initializer=tf.contrib.layers.xavier_initializer(), trainable=True)
        pre_activation = tf.nn.bias_add(tf.concat([conv1x1_1, conv3x3, conv5x5, conv1x1_4], 3), bias)
        y = tf.nn.relu(pre_activation)

    return y


def forward(images_batch, training):

    with tf.name_scope('inceptionnet'):
        # 1st Layer: Conv (w ReLu) -> Pool -> Norm
        conv1 = models_helper.conv2d(images_batch, 3, 16, 3, 1, 'SAME', True, 'conv1')
        pool1 = tf.nn.max_pool(conv1, ksize=[1, 3, 3, 1], strides=[1, 1, 1, 1], padding='SAME')

        inception1a = inception_block(pool1, 16, 16, 32, 8, 8, 8, 8, 'inception1a')
        inception1b = inception_block(inception1a, 64, 32, 48, 32, 24, 32, 32, 'inception1b')
        pool2 = tf.nn.max_pool(inception1b, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')

        inception2a = inception_block(pool2, 136, 48, 52, 68, 12, 68, 68, 'inception2a')
        inception2b = inception_block(inception2a, 180, 40, 56, 90, 16, 90, 90, 'inception2b')
        inception2c = inception_block(inception2b, 202, 32, 64, 101, 16, 101, 101, 'inception2c')
        inception2d = inception_block(inception2c, 213, 28, 72, 107, 16, 107, 107, 'inception2d')
        pool3 = tf.nn.max_pool(inception2d, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='SAME')

        inception3a = inception_block(pool3, 223, 64, 80, 111, 32, 111, 111, 'inception3a')
        inception3b = inception_block(inception3a, 287, 96, 96, 144, 32, 144, 144, 'inception3b')
        pool4 = tf.nn.avg_pool(inception3b, ksize=[1, 8, 8, 1], strides=[1, 1, 1, 1], padding='SAME')

        flattened = tf.reshape(pool4, [-1, 8 * 8 * 368])
        fc1 = models_helper.fc(flattened, 8 * 8 * 368, 10, 'fc1')

        
    return fc1

