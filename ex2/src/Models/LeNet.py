from Layers import *
from Optimization import *
import NeuralNetwork
import numpy as np


def build():
    net = NeuralNetwork.NeuralNetwork(Optimizers.Adam(5e-4, 0.98, 0.999, 1e-8),
                                      Initializers.Xavier(),
                                      Initializers.Constant(0.1))
    input_image_shape = (28, 28, 1)
    conv_stride_shape = (1, 1)
    categories = 10

    pool_stride_shape = (2, 2)
    pooling_shape = (2, 2)

    net.loss_layer = SoftMax.SoftMax()

    # net.append_trainable_layer(BatchNormalization.BatchNormalization())

    cl_1 = Conv.Conv(input_image_shape, conv_stride_shape, (3, 3, 1), 6)
    net.append_trainable_layer(cl_1)
    cl_1_output_shape = (*input_image_shape[:-1], 6)

    net.layers.append(ReLU.ReLU())

    net.layers.append(Pooling.Pooling(cl_1_output_shape, pool_stride_shape, pooling_shape))
    pooling_out_shape = (np.int(cl_1_output_shape[0] / pooling_shape[0]),
                         np.int(cl_1_output_shape[1] / pooling_shape[1]), 6)

    cl_2 = Conv.Conv(pooling_out_shape, conv_stride_shape, (3, 3, 6), 16)
    net.append_trainable_layer(cl_2)
    cl_2_output_shape = (*pooling_out_shape[:-1], 16)

    net.layers.append(ReLU.ReLU())

    net.layers.append(Pooling.Pooling(cl_2_output_shape, pool_stride_shape, pooling_shape))
    pooling_out_shape = (np.int(cl_2_output_shape[0] / pooling_shape[0]),
                         np.int(cl_2_output_shape[1] / pooling_shape[1]), 16)

    fcl_1_input_size = np.prod(pooling_out_shape)

    fcl_1 = FullyConnected.FullyConnected(fcl_1_input_size, 120)
    net.append_trainable_layer(fcl_1)

    net.layers.append(ReLU.ReLU())

    fcl_2 = FullyConnected.FullyConnected(120, 84)
    net.append_trainable_layer(fcl_2)
    
    net.layers.append(ReLU.ReLU())
    
    fcl_3 = FullyConnected.FullyConnected(84, categories)
    net.append_trainable_layer(fcl_3)
    
    net.layers.append(ReLU.ReLU())

    return net
