import numpy as np
from Layers.Base import Phase

class SoftMax:
    def __init__(self):
        self.Y = None
        self.weight_norm = 0
        self.phase = Phase.train

    @staticmethod
    def normalize_matrix(input_tensor):
        return input_tensor - np.max(input_tensor)

    def forward(self, input_tensor, label_tensor):
        self.Y = self.predict(input_tensor)

        t_label = label_tensor.T
        t_Y = self.Y.T
        loss = 0.0
        for i in range(label_tensor.shape[1]):
            for j in range(label_tensor.shape[0]):
                if t_label[i][j] == 1:
                    loss += (- np.log(t_Y[i][j]))
        return loss

    def backward(self, label_tensor):
        return self.Y - label_tensor

    def predict(self, input_tensor):
        n_input = np.exp(self.normalize_matrix(input_tensor))
        d = np.sum(n_input, axis=0)
        return n_input / d
