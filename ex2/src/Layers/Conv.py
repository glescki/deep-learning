import numpy as np
import scipy.signal
from copy import deepcopy
from Layers.Base import Phase
import math


class Conv:
    def __init__(self, input_image_shape, stride_shape, convolution_shape, num_kernels):
        self.input_image_shape = input_image_shape
        self.stride_shape = stride_shape
        self.convolution_shape = convolution_shape
        self.num_kernels = num_kernels
        self.delta = 1
        self.bias = np.ones(num_kernels)
        self.weights = np.random.uniform(0, 1, ((num_kernels,) + self.convolution_shape[::-1]))
        self.X = None
        self.optimizer = None
        self.weight_gradient = None
        self.bias_gradient = np.zeros(num_kernels)
        self.bias_optimizer = None
        self.weight_norm = 0
        self.phase = Phase.train

        self.output_shape = []
        image_spatial_shape = self.input_image_shape[:-1]
        for input_dim, stride in zip(image_spatial_shape, self.stride_shape):
            rounded = np.int(np.ceil(float(input_dim) / stride))
            self.output_shape.append(rounded)

    def initialize(self, weights_initializer, bias_initializer):
        for i in range(self.num_kernels):
            self.weights[i, :, :, :] = weights_initializer.initialize(self.weights[i, :, :, :])

        self.bias = bias_initializer.initialize(self.bias)

    def _forward1d(self, input_tensor):
        batch_size = input_tensor.shape[1]
        stride = self.stride_shape[0]
        l_input, c_input = self.input_image_shape
        l_output = len(list(range(0, l_input, stride)))

        output_shape = (l_output * self.num_kernels, input_tensor.shape[1])
        output = np.zeros(output_shape)

        for i in range(batch_size):
            img_input = input_tensor[:, i].reshape((c_input, l_input))
            A = np.zeros((self.num_kernels, l_input))

            for j in range(self.num_kernels):
                for k in range(c_input):
                    # Calculate convolution of each kernel into matrix A
                    A[j, :] += scipy.signal.convolve(img_input[k, :], self.weights[j, k, :], mode='same')
                # Add bias
                A[j, :] += self.bias[j]
            # Subsample matrix A according to stride size
            A = np.delete(A, np.setdiff1d(range(0, l_input), range(0, l_input, stride)), axis=1)

            output[:, i] = A.reshape(output_shape[0:-1])
        self.X = input_tensor
        if self.optimizer is not None:
            if self.optimizer.regularizer is not None:
                for c in range(self.num_kernels):
                    self.weight_norm += self.optimizer.regularizer.norm(self.weights[c])

        return output

    def forward(self, input_tensor):
        if len(self.input_image_shape) == 2:
            return self._forward1d(input_tensor)

        batch_size = input_tensor.shape[1]
        h_stride, w_stride = self.stride_shape
        h_input, w_input, c_input = self.input_image_shape

        output_shape = (self.output_shape[0] * self.output_shape[1] * self.num_kernels, batch_size)
        output = np.zeros(output_shape)
        for i in range(batch_size):
            img_input = input_tensor[:, i].reshape((c_input, w_input, h_input))
            A = np.zeros((self.num_kernels, w_input, h_input))
            for j in range(self.num_kernels):
                for k in range(c_input):
                    # Calculate convolution of each kernel into matrix A
                    A[j, :, :] += scipy.signal.convolve2d(img_input[k, :, :], self.weights[j, k, :, :], mode='same')
                # Add bias
                A[j, :, :] += self.bias[j]
            A = np.delete(A, np.setdiff1d(range(0, h_input), range(0, h_input, h_stride)), axis=2)
            A = np.delete(A, np.setdiff1d(range(0, w_input), range(0, w_input, w_stride)), axis=1)
            output[:, i] = A.reshape(output_shape[0:-1])
        self.X = input_tensor
        if self.optimizer is not None:
            if self.optimizer.regularizer is not None:
                for j in range(self.num_kernels):
                    for k in range(c_input):
                        self.weight_norm += self.optimizer.regularizer.norm(self.weights[j, k, :, :])
        return output

    def _backward1d(self, error_tensor):

        batch_size = error_tensor.shape[1]
        stride = self.stride_shape[0]
        l_input, c_input = self.input_image_shape

        grad_input_shape = (l_input * c_input, batch_size)
        grad_input = np.zeros(grad_input_shape)

        pad = np.int(math.floor(self.convolution_shape[0] / 2))

        self.bias_gradient = np.zeros_like(self.bias_gradient)
        self.weight_gradient = np.zeros(self.weights.shape)

        for i in range(batch_size):
            # First we calculate the new weight
            error = error_tensor[:, i].reshape((self.num_kernels, self.output_shape[0]))

            X = self.X[:, i].reshape((c_input, l_input))

            # Upsample error
            x = list(range(0, l_input, stride))
            _error = np.zeros((self.num_kernels, l_input))
            for l in range(self.num_kernels):
                _j = 0
                for j in x:
                    _error[l, j] = error[l, _j]
                _j += 1

            # Pads X with half the weight shape
            npad = ((0, 0), (pad, pad - np.mod(self.convolution_shape[0] + 1, 2)))
            X_pad = np.lib.pad(X, npad, 'constant', constant_values=0.0)

            # Correlation of X_pad with error generating weight gradient
            for j in range(self.num_kernels):
                for k in range(c_input):
                    self.weight_gradient[j, k, :] += scipy.signal.correlate(X_pad[k, :], _error[j, :], mode='valid')

            # Calculating bias gradient
            for j in range(self.num_kernels):
                self.bias_gradient[j] += np.sum(_error[j])

            # Then we calculate the output of the backward pass
            A = np.zeros((c_input, l_input))
            for j in range(c_input):
                for k in range(self.num_kernels):
                    A[j, :] += scipy.signal.correlate(_error[k, :], self.weights[k, j, :], mode='same')

            grad_input[:, i] = A.reshape(l_input * c_input)

        # Flip gradient
        for j in range(self.num_kernels):
            for k in range(c_input):
                self.weight_gradient[j, k, :] = self.weight_gradient[j, k, ::-1]

        if self.optimizer is not None:
            self.weights = self.optimizer.calculate_update(self.delta, self.weights, self.weight_gradient)
            self.bias = self.bias_optimizer.calculate_update(self.delta, self.bias, self.bias_gradient)

        return grad_input

    def backward(self, error_tensor):
        if len(self.input_image_shape) == 2:
            return self._backward1d(error_tensor)

        batch_size = error_tensor.shape[1]
        h_stride, w_stride = self.stride_shape
        h_input, w_input, c_input = self.input_image_shape

        grad_input_shape = (h_input * w_input * c_input, batch_size)
        grad_input = np.zeros(grad_input_shape)

        self.bias_gradient = np.zeros_like(self.bias_gradient)
        self.weight_gradient = np.zeros(self.weights.shape)

        for i in range(batch_size):
            # First we calculate the new weight
            error = error_tensor[:, i].reshape((self.num_kernels, self.output_shape[1], self.output_shape[0]))

            X = self.X[:, i].reshape((c_input, w_input, h_input))

            # Upsample error
            x = list(range(0, h_input, h_stride))
            y = list(range(0, w_input, w_stride))
            _error = np.zeros((self.num_kernels, w_input, h_input))
            for l in range(self.num_kernels):
                _j = 0
                for j in x:
                    _k = 0
                    for k in y:
                        _error[l, k, j] = error[l, _k, _j]
                        _k += 1
                    _j += 1

            # Pads X with half the weight shape
            padding = tuple((np.int(np.floor(x / 2.)),
                             np.int(np.floor((x / 2.) - 0.49)))
                            for x in self.weights.shape[2:])
            padding = list(padding)
            padding.insert(0, (0, 0))
            padding = tuple(padding)
            X_pad = np.lib.pad(X, padding, 'constant', constant_values=0.0)

            # Correlation of X_pad with error generating weight gradient
            for j in range(self.num_kernels):
                for k in range(c_input):
                    self.weight_gradient[j, k, :, :] += scipy.signal.correlate2d(X_pad[k, :, :], _error[j, :, :],
                                                                                 mode='valid')

            # Calculating bias gradient
            for j in range(self.num_kernels):
                self.bias_gradient[j] += np.sum(_error[j])

            # Then we calculate the output of the backward pass
            A = np.zeros((c_input, w_input, h_input))
            for j in range(c_input):
                for k in range(self.num_kernels):
                    A[j, :, :] += scipy.signal.correlate2d(_error[k, :, :], self.weights[k, j, :, :], mode='same')

            grad_input[:, i] = A.reshape(h_input * w_input * c_input)

        # Flip gradient
        for j in range(self.num_kernels):
            for k in range(c_input):
                self.weight_gradient[j, k, :, :] = self.weight_gradient[j, k, ::-1, ::-1]

        if self.optimizer is not None:
            self.weights = self.optimizer.calculate_update(self.delta, self.weights, self.weight_gradient)
            self.bias = self.bias_optimizer.calculate_update(self.delta, self.bias, self.bias_gradient)

        return grad_input

    def set_optimizer(self, optimizer):
        self.optimizer = optimizer
        self.bias_optimizer = deepcopy(optimizer)

    def get_gradient_weights(self):
        return self.weight_gradient

    def get_gradient_bias(self):
        return self.bias_gradient
