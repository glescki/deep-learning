from enum import Enum


class Phase(Enum):
    train = 0
    test = 1
    validation = 2
