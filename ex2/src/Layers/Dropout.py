import numpy as np
from Layers.Base import Phase


class Dropout:
    def __init__(self, probability):
        self.phase = Phase.train
        self.p = probability
        self.mask = None
        self.weight_norm = 0

    def forward(self, input_tensor):
        if self.phase == Phase.train:
            self.mask = np.random.binomial(1, (1 - self.p), size=input_tensor.shape)
            return (input_tensor * self.mask) / self.p
            
        return input_tensor

    def backward(self, error_tensor):
        return error_tensor * self.mask
