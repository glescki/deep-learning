import numpy as np
from Layers.Base import Phase

class Pooling:
    def __init__(self, img_shape, stride, pooling_shape):
        self.stride = stride
        self.pooling_shape = pooling_shape
        self.img_shape = img_shape
        self.weight_norm = 0
        self.phase = Phase.train

        # calculate the output shape
        self.out_shape = []
        for input_dim, pooling_dim, stride in zip(img_shape[:-1], pooling_shape, self.stride):
            rounded = np.int(np.floor(np.float(input_dim - pooling_dim) / np.float(stride)) + 1)
            self.out_shape.append(rounded)

        self.out_shape = (tuple(self.out_shape) + (self.img_shape[-1],))

        self.switches = None
        self.this_input_shape = None

    def forward(self, input_tensor):

        batch_size = input_tensor.shape[1]
        self.this_input_shape = (tuple(reversed(self.img_shape)) + (batch_size,))
        input_tensor = input_tensor.reshape(self.this_input_shape)

        result = np.zeros((tuple(reversed(self.out_shape)) + (batch_size,)))
        self.switches = {}
        for this_batch in range(batch_size):
            for this_channel in range(self.img_shape[-1]):
                y_range = range(0, self.out_shape[1], self.stride[1])
                for y_out, y_in in enumerate(y_range):
                    x_range = range(0, self.out_shape[0], self.stride[0])
                    for x_out, x_in in enumerate(x_range):
                        input_part = input_tensor[
                                     this_channel,
                                     y_in:y_in + self.pooling_shape[1],
                                     x_in:x_in + self.pooling_shape[0],
                                     this_batch]
                        max_idx = np.unravel_index(
                            input_part.argmax(),
                            input_part.shape)

                        shape = (this_channel, y_out, x_out, this_batch)
                        self.switches[shape] = (max_idx[0] + y_in, max_idx[1] + x_in)

                        result[shape] = input_part[max_idx]
        result = result.reshape(np.prod(self.out_shape), batch_size)
        return result

    def backward(self, error_tensor):
        batch_size = error_tensor.shape[1]
        error_below = np.zeros(self.this_input_shape)
        error_tensor = error_tensor.reshape(tuple(reversed(self.out_shape)) + (batch_size,))

        for this_batch in range(batch_size):
            for this_channel in range(self.img_shape[-1]):
                y_range = range(0, self.out_shape[1], self.stride[1])
                for y_out, y_in in enumerate(y_range):
                    x_range = range(0, self.out_shape[0], self.stride[0])
                    for x_out, x_in in enumerate(x_range):
                        shape = (this_channel, y_out, x_out, this_batch)
                        error_below[this_channel, self.switches[shape][0], self.switches[shape][1], this_batch] += error_tensor[shape]

        error_below = error_below.reshape(np.prod(self.img_shape), batch_size)
        return error_below
