import numpy as np
import copy
import pickle
from Layers import Base


def save(filename, net):
    try:
        f = open(filename)
    except IOError:
        # If not exists, create the file
        f = open(filename, 'w+')
    pickle.dump(net, f)


def load(filename, data_layer):
    f = open(filename, 'r')
    data_layer = pickle.load(f)
    return data_layer


class NeuralNetwork:
    def __init__(self, optimizer, weights_initializer, bias_initializer):
        self.loss = []
        self.layers = []
        self.data_layer = None
        self.loss_layer = None
        self.weight_norm = None
        self.weights_initializer = weights_initializer
        self.bias_initializer = bias_initializer
        self.optimizer = optimizer

    def forward(self, activation_tensor):
        for layer in self.layers:
            activation_tensor = layer.forward(activation_tensor)
            self.weight_norm += layer.weight_norm
        return activation_tensor

    def backward(self, error_tensor):
        for layer in reversed(self.layers):
            error_tensor = layer.backward(error_tensor)
        return error_tensor

    def append_trainable_layer(self, layer):
        layer.initialize(self.weights_initializer, self.bias_initializer)
        layer.set_optimizer(copy.deepcopy(self.optimizer))
        self.layers.append(layer)
        
    def set_phase(self, phase):
        for layer in self.layers:
            layer.phase = phase

    def train(self, iterations):
        self.set_phase(Base.Phase.train)
        for i in range(iterations):
            self.weight_norm = 0.0
            input_tensor, label_tensor = self.data_layer.forward()
            activation_tensor = self.forward(input_tensor)

            loss = self.loss_layer.forward(activation_tensor, label_tensor)
            self.loss.append(loss + self.weight_norm)

            error_tensor = self.loss_layer.backward(label_tensor)

            self.backward(error_tensor)

    def test(self, input_tensor):
        self.set_phase(Base.Phase.test)
        activation_tensor = self.forward(input_tensor)
        loss = self.loss_layer.predict(activation_tensor)

        return loss
