import numpy as np
from Optimization.Base import Base


class Sgd(Base):
    def __init__(self, global_delta):
        Base.__init__(self)
        self.global_delta = global_delta
        self.regularizer = None

    def calculate_update(self, individual_delta, weight_tensor, gradient_tensor):
        delta = individual_delta * self.global_delta

        new_weight = weight_tensor - gradient_tensor * delta

        if self.regularizer is not None:
            new_weight -= delta * self.regularizer.calculate(weight_tensor)

        return new_weight


class SgdWithMomentum(Base):

    def __init__(self, global_delta, mu):
        Base.__init__(self)
        self.global_delta = global_delta
        self.mu = mu
        self.v = 0.0
        self.regularizer = None

    def calculate_update(self, individual_delta, weight_tensor, gradient_tensor):
        delta = individual_delta * self.global_delta
        self.v = (self.mu * self.v) - (delta * gradient_tensor)

        new_weight = weight_tensor + self.v

        if self.regularizer is not None:
            new_weight -= delta * self.regularizer.calculate(weight_tensor)

        return new_weight


class Adam(Base):
    def __init__(self, global_delta, mu, rho, epsilon):
        Base.__init__(self)
        self.global_delta = global_delta
        self.mu = mu
        self.rho = rho
        self.epsilon = epsilon
        self.v = 0.0
        self.r = 0.0
        self.k = 1
        self.regularizer = None

    def calculate_update(self, individual_delta, weight_tensor, gradient_tensor):
        delta = individual_delta * self.global_delta

        self.v = (self.mu * self.v) + np.multiply((1.0 - self.mu), gradient_tensor)
        self.r = (self.rho * self.r) + np.multiply((1.0 - self.rho), np.square(gradient_tensor))

        v = np.true_divide(self.v, 1.0 - np.power(self.mu, self.k))
        r = np.true_divide(self.r, 1.0 - np.power(self.rho, self.k))

        self.k += 1

        new_weight = weight_tensor - delta * np.true_divide((v + self.epsilon), (np.sqrt(r) + self.epsilon))

        if self.regularizer is not None:
            new_weight -= delta * self.regularizer.calculate(weight_tensor)

        return new_weight
